import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
// import {MatDialogModule} from "@angular/material";


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatCardModule } from "@angular/material/card";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatButtonModule } from "@angular/material/button";
import { FlexLayoutModule } from "@angular/flex-layout";
import { TestComponent } from './test/test.component';
 import { MatSliderModule } from '@angular/material/slider';
import { VoitureComponent } from './cars/voiture/voiture.component';
import { VoitureDetailComponent } from './cars/voiture/voiture-detail/voiture-detail.component';
import {MatIconModule} from '@angular/material/icon'
import { StarRatingModule } from 'angular-star-rating';
import { FormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { ReactiveFormsModule } from '@angular/forms';
import {MatBadgeModule} from '@angular/material/badge';
import {MatSelectModule} from '@angular/material/select';
import { MatDialogModule} from '@angular/material/dialog';
import { AjouterVoitureComponent } from './cars/voiture/ajouter-voiture/ajouter-voiture.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ModifierVoitureComponent } from './cars/voiture/modifier-voiture/modifier-voiture.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';


@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    VoitureComponent,
    VoitureDetailComponent,
    AjouterVoitureComponent,
    ModifierVoitureComponent

  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
   HttpClientModule,
   MatDialogModule,
   MatFormFieldModule,
    MatCardModule,
    MatToolbarModule,
    MatButtonModule,
    FlexLayoutModule,
    MatSliderModule,
    StarRatingModule.forRoot(),
    MatIconModule,
  
    FormsModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    StarRatingModule,
    MatBadgeModule,
    MatSelectModule,
    MatInputModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatPaginatorModule,
    MatNativeDateModule,
    MatTooltipModule
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
