import { Component, Output } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
 mtBadge:any=0;
  @Output() showlist: boolean;

  constructor(private router: Router){
    window.addEventListener('mtBadge', (evt: any) => {
      this.mtBadge = this.mtBadge +1;
      // evt.detail
      console.log("evt.detail",evt.detail)

     
    }
    );
  }
  show(){
   this.showlist=true;
   console.log("cliked")
   console.log("showlist",this.showlist)
  }
  refrechAccuil(){
    this.router.navigate(['']);

    this.showlist=false;
  }
}
