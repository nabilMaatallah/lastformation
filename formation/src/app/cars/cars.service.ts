import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CarsService {


  constructor(private http: HttpClient) { }
  getCars(): Observable<any> {
    return this.http.get(environment.api + '/voitures') as Observable<any>;
    }
    postCars(obj:any): Observable<any> {
      console.log("objet a ajouter",obj)
      return this.http.post(environment.api + '/voitures',obj) as Observable<any>;
      }
      deleteCars(ide:any): Observable<any> {
        console.log("objet a supprimer",ide)
        return this.http.delete(environment.api + '/voitures/'+JSON.stringify(ide)) as Observable<any>;
        }

        patchCars(obj:any): Observable<any> {
          console.log("objet a modifier",obj)
          return this.http.patch(environment.api + '/voitures/'+JSON.stringify(obj.id),obj) as Observable<any>;
  
          
          }

}
