import { Component, OnInit,Input } from '@angular/core';
import {AppComponent} from 'src/app/app.component';
import {MatDialog} from '@angular/material/dialog';

import { CarsService } from '../cars.service';
import { AjouterVoitureComponent } from './ajouter-voiture/ajouter-voiture.component';
import { ModifierVoitureComponent } from './modifier-voiture/modifier-voiture.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import{SupprimerVoitureComponent} from './supprimer-voiture/supprimer-voiture.component';
import { PageEvent } from '@angular/material/paginator';



@Component({
  selector: 'app-voiture',
  templateUrl: './voiture.component.html',
  styleUrls: ['./voiture.component.css']
})
export class VoitureComponent implements OnInit {
  open: boolean=false;
  data: any;
  @Input()showlist:any

  constructor (private carsservice: CarsService,private apComponent:AppComponent,public dialog: MatDialog,private _snackBar: MatSnackBar) { }
  object: any;
  public pageSlice:any

  ngOnInit () {
    this.carsservice.getCars().subscribe(
      data => {
        this.object = data;	
        this.pageSlice=this.object.slice(0,4);
      }
    );
    this.showlist=this.apComponent.showlist;
  }
 
  show(element){ 
    this.data=element
    this.showlist=false;
    }


    openDialogAjout() {
      const dialogRef = this.dialog.open(AjouterVoitureComponent);
      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
        this.ngOnInit ();
      });
    }

    openDialogModifier(iden: any) {
      const dialogRef = this.dialog.open(ModifierVoitureComponent,{data:iden});
      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
        this.ngOnInit ();
      });
    }
    openDialogSupprimer(iden: any){
      console.log("id de voiture a supprimer",iden)
      const dialogRef = this.dialog.open(SupprimerVoitureComponent,{data:iden});
      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
        this.ngOnInit ();
      });
    }
    OnPageChange(event: PageEvent){
      const startIndex=event.pageIndex*event.pageSize;
      let endIndex=startIndex+event.pageSize;
      if(endIndex>this.object.length){
        endIndex=this.object.length;
      }
      this.pageSlice=this.object.slice(startIndex,endIndex);
    }

    
    
}
