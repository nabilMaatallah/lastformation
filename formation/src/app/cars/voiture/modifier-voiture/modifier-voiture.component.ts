import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { voiture } from '../voiture';
import { CarsService } from '../../cars.service';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-modifier-voiture',
  templateUrl: './modifier-voiture.component.html',
  styleUrls: ['./modifier-voiture.component.css']
})
export class ModifierVoitureComponent implements OnInit {

  maxDate=new Date();
  dateFilter= date =>{
    const day= date.getDay();
    return day !=0 && day !=6;
  }
  objVoiture = new  voiture();
  modifObjVoiture = new  voiture();
  marque:string
  model:string
  prix:number
  formed:any
  motor:string
  couleur:string
  image:string
  
  constructor( @Inject(MAT_DIALOG_DATA) public data: any,private carsservice: CarsService,private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.objVoiture=this.data;
    this.marque=this.objVoiture.marque;
    this.model=this.objVoiture.model;
    this.prix = this.objVoiture.prix;
    this.formed=this.objVoiture.formed;
    this.motor=this.objVoiture.motor;
    this.couleur=this.objVoiture.couleur;
    this.image=this.objVoiture.image;

    console.log("obj",this.objVoiture);

  }
  patchVoiture(){ 
    this.modifObjVoiture.id=this.objVoiture.id;
    this.modifObjVoiture.marque=this.marque;
     this.modifObjVoiture.model=this.model;
     this.modifObjVoiture.prix=+this.prix;
     this.modifObjVoiture.formed=this.formed;
     this.modifObjVoiture.motor=this.motor;
     this.modifObjVoiture.couleur=this.couleur;
     this.modifObjVoiture.image=this.image;
     console.log('modif obj',this.modifObjVoiture)
     this.carsservice.patchCars(this.modifObjVoiture).subscribe(
      ()=>{
        this._snackBar.open("modification  validééééé", "", {
          duration: 4000,
          verticalPosition: 'top',
          horizontalPosition: 'center',
          panelClass: ['alert-red'],
        
        });
        console.log('modification  validééééé')
      }
    );
 
  }

}
