import { Component, OnInit, Input, Output,EventEmitter} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import {MatSelectModule} from '@angular/material/select';
import { panier } from '../voiture';

// import { tabPanier } from '../voiture';



@Component({
  selector: 'app-voiture-detail',
  templateUrl: './voiture-detail.component.html',
  styleUrls: ['./voiture-detail.component.css']
})
export class VoitureDetailComponent implements OnInit 
{
  rating:number = 3;
  // starCount:number = 5;
  // starColor:string="accent";
  // starColorP:string = "accent";
  // starColorW:string =  "warn";
  vr:any


  @Input()data:any
  @Input()showlist:any

  mtBadge:number;
  @Output() messageEvent = new EventEmitter<number>();

   panier = new  panier()

  //  tabPanier = new tabPanier();
  tabPanier :panier []= [];
  public selected =1;


  id: number;
  private sub: any;
 
  dat: any;

 

  constructor(private route: ActivatedRoute,private httpService: HttpClient) 
  { 
   
  }
  obj: any;
  VoiD:any
  ngOnInit(): void {

   
    
    //recuperation de valeur du parametre
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number

      // In a real app: dispatch action to load the details here.
   });
   console.log("identifiat du voiture",this.id)

  //recuperation de tous les element du fichier json 
   this.httpService.get('./assets/Database.json').subscribe(
    dat => {
      this.obj = dat;	 
      console.log("liste des objets",this.obj)
      console.log("identifiat du voiture",this.obj.length)

    this.VoiD=this.obj[0];
     var i=0;
      while(i<this.obj.length&&(this.obj[i]['id']!=this.id))
      {
        i++;
        this.VoiD=this.obj[i];
          
      }
   
    }
  );


    
  }

  showIcon(index:number) 
  {
    if (this.rating >= index + 1) 
    {
      return 'star';
    } 
    else
     {
      return 'star_border';
    }
  }

  ajoutPanier(){
    this.mtBadge=this.tabPanier.length+1;
   
    console.log("mtBadge ",this.mtBadge);

    console.log("les id ",this.id);
    console.log("tableau de panier ",this.tabPanier);

    this.panier.idProduit=this.id;
    this.panier.quantite=this.selected;

    this.tabPanier.push(this.panier);
    console.log("tableau de panier ",this.tabPanier);
    console.log("mtBadge ",this.mtBadge);

    var mtBadge = new CustomEvent('mtBadge' , { 'detail': this.mtBadge});
    window.dispatchEvent(mtBadge);


  }

  
  changeClient()
  {
    console.log("value",this.selected);
  }
   
  

}
