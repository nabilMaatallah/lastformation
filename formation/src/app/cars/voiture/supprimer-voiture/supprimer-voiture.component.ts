import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CarsService } from '../../cars.service';

@Component({
  selector: 'app-supprimer-voiture',
  templateUrl: './supprimer-voiture.component.html',
  styleUrls: ['./supprimer-voiture.component.css']
})
export class SupprimerVoitureComponent implements OnInit {
  idenVoiture: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,private carsservice: CarsService,private _snackBar: MatSnackBar,public dialogRef: MatDialogRef<SupprimerVoitureComponent>) { }

  ngOnInit(): void {
    this.idenVoiture=this.data;

  }


  suppVoiture(){


    this.carsservice.deleteCars(this.idenVoiture).subscribe(
        ()=>{
          this._snackBar.open("supprission  validééééé", "", {
            duration: 4000,
            verticalPosition: 'top',
          horizontalPosition: 'center',
          panelClass: ['alert-red'],
            
          });
          console.log('supprision validééééé')
        }
      );
  }

  cancel(){    
    this.dialogRef.close();
  }

}
