import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupprimerVoitureComponent } from './supprimer-voiture.component';

describe('SupprimerVoitureComponent', () => {
  let component: SupprimerVoitureComponent;
  let fixture: ComponentFixture<SupprimerVoitureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupprimerVoitureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SupprimerVoitureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
