import { Component, OnInit } from '@angular/core';
import { CarsService } from '../../cars.service';
import { voiture } from '../voiture';
import {MatSnackBar} from '@angular/material/snack-bar';
import { jitOnlyGuardedExpression } from '@angular/compiler/src/render3/util';


@Component({
  selector: 'app-ajouter-voiture',
  templateUrl: './ajouter-voiture.component.html',
  styleUrls: ['./ajouter-voiture.component.css']
})
export class AjouterVoitureComponent implements OnInit {
  maxDate=new Date();
  dateFilter= date =>{
    const day= date.getDay();
    return day !=0 && day !=6;
  }

  objVoiture = new  voiture();
  marque:string
  model:string
  prix:string
  formed:any
  motor:string
  couleur:string
  image:string
  constructor(private carsservice: CarsService,private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }
   postVoiture(){

    console.log("formed",this.formed)
     this.objVoiture.marque=this.marque;
     this.objVoiture.model=this.model;
     this.objVoiture.prix=+this.prix;
     this.objVoiture.formed=this.formed;
     this.objVoiture.motor=this.motor;
     this.objVoiture.couleur=this.couleur;
     this.objVoiture.image=this.image;
     console.log('formed', this.formed) 
     console.log('obj formed', this.objVoiture.formed) 
    console.log("information du voiture ",this.objVoiture)
    this.carsservice.postCars(this.objVoiture).subscribe(
      ()=>{  
        this._snackBar.open("ajout avec succées", "", {
          duration: 4000,
          verticalPosition: 'top',
          horizontalPosition: 'center',
          panelClass: ['alert-red'],
        });
        console.log('ajout validééééé') 

      }
    );
  }

}
