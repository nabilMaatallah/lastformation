import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VoitureComponent } from './voiture/voiture.component';
import { VoitureDetailComponent } from './voiture/voiture-detail/voiture-detail.component';
import { AjouterVoitureComponent } from './voiture/ajouter-voiture/ajouter-voiture.component';
import { ModifierVoitureComponent } from './voiture/modifier-voiture/modifier-voiture.component';
import { SupprimerVoitureComponent } from './voiture/supprimer-voiture/supprimer-voiture.component';



@NgModule({
  declarations: [VoitureComponent,
     VoitureDetailComponent, AjouterVoitureComponent,
      ModifierVoitureComponent, SupprimerVoitureComponent],
  imports: [
    CommonModule
    
  ]
})
export class CarsModule { }
