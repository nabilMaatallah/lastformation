import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VoitureComponent } from './cars/voiture/voiture.component';
import { VoitureDetailComponent } from './cars/voiture/voiture-detail/voiture-detail.component';

import { AppComponent } from './app.component';


const routes: Routes = [
{ path: 'detail/:id', component: VoitureDetailComponent}, 
{ path: 'voiture-component', component: VoitureComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
