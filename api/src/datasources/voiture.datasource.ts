import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';
import * as config from './voiture.datasource.config.json';


@lifeCycleObserver('datasource')
export class VoitureDataSource extends juggler.DataSource
  {
  static dataSourceName = 'voiture';

  constructor(
    @inject('datasources.config.voiture', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
