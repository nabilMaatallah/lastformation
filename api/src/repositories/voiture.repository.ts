import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {Voiture, VoitureRelations, Owner, Categorie} from '../models';
import {VoitureDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {OwnerRepository} from './owner.repository';
import {CategorieRepository} from './categorie.repository';

export class VoitureRepository extends DefaultCrudRepository<
  Voiture,
  typeof Voiture.prototype.id,
  VoitureRelations
> {

  public readonly owner: BelongsToAccessor<Owner, typeof Voiture.prototype.id>;

  public readonly categorie: BelongsToAccessor<Categorie, typeof Voiture.prototype.id>;

  constructor(
    @inject('datasources.voiture') dataSource: VoitureDataSource, @repository.getter('OwnerRepository') protected ownerRepositoryGetter: Getter<OwnerRepository>, @repository.getter('CategorieRepository') protected categorieRepositoryGetter: Getter<CategorieRepository>,
  ) {
    super(Voiture, dataSource);
    this.categorie = this.createBelongsToAccessorFor('categorie', categorieRepositoryGetter,);
    this.registerInclusionResolver('categorie', this.categorie.inclusionResolver);
    this.owner = this.createBelongsToAccessorFor('owner', ownerRepositoryGetter,);
    this.registerInclusionResolver('owner', this.owner.inclusionResolver);
  }
}
