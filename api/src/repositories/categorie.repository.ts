import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {Categorie, CategorieRelations, Voiture} from '../models';
import {VoitureDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {VoitureRepository} from './voiture.repository';

export class CategorieRepository extends DefaultCrudRepository<Categorie,typeof Categorie.prototype.id,CategorieRelations> {

  public readonly voitures: HasManyRepositoryFactory<Voiture, typeof Categorie.prototype.id>;

  constructor(
    @inject('datasources.voiture') dataSource: VoitureDataSource, @repository.getter('VoitureRepository') protected voitureRepositoryGetter: Getter<VoitureRepository>,
  ) {
    super(Categorie, dataSource);
    
    this.voitures = this.createHasManyRepositoryFactoryFor('voitures', voitureRepositoryGetter,);
    this.registerInclusionResolver('voitures', this.voitures.inclusionResolver);
  }
}
