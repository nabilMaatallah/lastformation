import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {Owner, OwnerRelations, Voiture} from '../models';
import {inject, Getter} from '@loopback/core';
import {VoitureRepository} from './voiture.repository';
import { VoitureDataSource } from '../datasources';

export class OwnerRepository extends DefaultCrudRepository<Owner,typeof Owner.prototype.age,OwnerRelations> {

  public readonly voitures: HasManyRepositoryFactory<Voiture, typeof Owner.prototype.age>;

  constructor(
    @inject('datasources.owner') dataSource: VoitureDataSource,
     @repository.getter('VoitureRepository') protected voitureRepositoryGetter: Getter<VoitureRepository>, 
  ) {
    super(Owner, dataSource);
    this.voitures = this.createHasManyRepositoryFactoryFor('voitures', voitureRepositoryGetter,);
    this.registerInclusionResolver('voitures', this.voitures.inclusionResolver);

  }
}
