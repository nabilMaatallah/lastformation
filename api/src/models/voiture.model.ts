import {Entity, model, property, belongsTo} from '@loopback/repository';
import {Owner} from './owner.model';
import {Categorie} from './categorie.model';

@model({settings: {strict: false}})
export class Voiture extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  marque?: string;

  @property({
    type: 'string',
    required: true,
  })
  model: string;

  @property({
    type: 'number',
    required: true,
  })
  prix: number;

  @property({
    type: 'string',
    required: true,
  })
  formed: string;

  @property({
    type: 'string',
    required: true,
  })
  motor: string;

  @property({
    type: 'string',
  })
  image: string;

  @property({
    type: 'string',
    required: true,
  })
  couleur: string;

  @belongsTo(() => Owner)
  ownerId: number;

  @belongsTo(() => Categorie)
  categorieId: number;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Voiture>) {
    super(data);
  }
}

export interface VoitureRelations {
  // describe navigational properties here
}

export type VoitureWithRelations = Voiture & VoitureRelations;
