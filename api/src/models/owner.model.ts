import {Entity, model, property, belongsTo, hasMany} from '@loopback/repository';
import {Voiture} from './voiture.model';

@model({settings: {strict: false}})
export class Owner extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  age?: number;

  @property({
    type: 'date',
    required: true,
  })

  datenaiss: Date;

  @property({
    type: 'number',
    required: true,
  })
  numcatreblue: number;

  @hasMany(() => Voiture)
  voitures: Voiture[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Owner>) {
    super(data);
  }
}

export interface OwnerRelations {
  // describe navigational properties here
}

export type OwnerWithRelations = Owner & OwnerRelations;
