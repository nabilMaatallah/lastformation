import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Voiture,
  Categorie,
} from '../models';
import {VoitureRepository} from '../repositories';

export class VoitureCategorieController {
  constructor(
    @repository(VoitureRepository)
    public voitureRepository: VoitureRepository,
  ) { }

  @get('/voitures/{id}/categorie', {
    responses: {
      '200': {
        description: 'Categorie belonging to Voiture',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Categorie)},
          },
        },
      },
    },
  })
  async getCategorie(
    @param.path.number('id') id: typeof Voiture.prototype.id,
  ): Promise<Categorie> {
    return this.voitureRepository.categorie(id);
  }
}
