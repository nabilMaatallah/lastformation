import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Voiture,
  Owner,
} from '../models';
import {VoitureRepository} from '../repositories';

export class VoitureOwnerController {
  constructor(
    @repository(VoitureRepository)
    public voitureRepository: VoitureRepository,
  ) { }

  @get('/voitures/{id}/owner', {
    responses: {
      '200': {
        description: 'Owner belonging to Voiture',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Owner)},
          },
        },
      },
    },
  })
  async getOwner(
    @param.path.number('id') id: typeof Voiture.prototype.id,
  ): Promise<Owner> {
    return this.voitureRepository.owner(id);
  }
}
