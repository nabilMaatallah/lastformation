import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Owner,
  Voiture,
} from '../models';
import {OwnerRepository} from '../repositories';

export class OwnerVoitureController {
  constructor(
    @repository(OwnerRepository) protected ownerRepository: OwnerRepository,
  ) { }

  @get('/owners/{id}/voitures', {
    responses: {
      '200': {
        description: 'Array of Owner has many Voiture',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Voiture)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Voiture>,
  ): Promise<Voiture[]> {
    return this.ownerRepository.voitures(id).find(filter);
  }

  @post('/owners/{id}/voitures', {
    responses: {
      '200': {
        description: 'Owner model instance',
        content: {'application/json': {schema: getModelSchemaRef(Voiture)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Owner.prototype.age,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Voiture, {
            title: 'NewVoitureInOwner',
            exclude: ['id'],
            optional: ['ownerId']
          }),
        },
      },
    }) voiture: Omit<Voiture, 'id'>,
  ): Promise<Voiture> {
    return this.ownerRepository.voitures(id).create(voiture);
  }

  @patch('/owners/{id}/voitures', {
    responses: {
      '200': {
        description: 'Owner.Voiture PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Voiture, {partial: true}),
        },
      },
    })
    voiture: Partial<Voiture>,
    @param.query.object('where', getWhereSchemaFor(Voiture)) where?: Where<Voiture>,
  ): Promise<Count> {
    return this.ownerRepository.voitures(id).patch(voiture, where);
  }

  @del('/owners/{id}/voitures', {
    responses: {
      '200': {
        description: 'Owner.Voiture DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Voiture)) where?: Where<Voiture>,
  ): Promise<Count> {
    return this.ownerRepository.voitures(id).delete(where);
  }
}
