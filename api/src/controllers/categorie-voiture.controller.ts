import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Categorie,
  Voiture,
} from '../models';
import {CategorieRepository} from '../repositories';

export class CategorieVoitureController {
  constructor(
    @repository(CategorieRepository) protected categorieRepository: CategorieRepository,
  ) { }

  @get('/categories/{id}/voitures', {
    responses: {
      '200': {
        description: 'Array of Categorie has many Voiture',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Voiture)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Voiture>,
  ): Promise<Voiture[]> {
    return this.categorieRepository.voitures(id).find(filter);
  }

  @post('/categories/{id}/voitures', {
    responses: {
      '200': {
        description: 'Categorie model instance',
        content: {'application/json': {schema: getModelSchemaRef(Voiture)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Categorie.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Voiture, {
            title: 'NewVoitureInCategorie',
            exclude: ['id'],
            optional: ['categorieId']
          }),
        },
      },
    }) voiture: Omit<Voiture, 'id'>,
  ): Promise<Voiture> {
    return this.categorieRepository.voitures(id).create(voiture);
  }

  @patch('/categories/{id}/voitures', {
    responses: {
      '200': {
        description: 'Categorie.Voiture PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Voiture, {partial: true}),
        },
      },
    })
    voiture: Partial<Voiture>,
    @param.query.object('where', getWhereSchemaFor(Voiture)) where?: Where<Voiture>,
  ): Promise<Count> {
    return this.categorieRepository.voitures(id).patch(voiture, where);
  }

  @del('/categories/{id}/voitures', {
    responses: {
      '200': {
        description: 'Categorie.Voiture DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Voiture)) where?: Where<Voiture>,
  ): Promise<Count> {
    return this.categorieRepository.voitures(id).delete(where);
  }
}
